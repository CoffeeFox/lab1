import timeit
import random
from memory_profiler import profile

bubble_memory_usage = open('memory_usage/bubble_memory_usage.txt', 'w')
insertion_memory_usage = open('memory_usage/insertion_memory_usage.txt', 'w')
shell_memory_usage = open('memory_usage/shell_memory_usage.txt', 'w')
quick_memory_usage = open('memory_usage/quick_memory_usage.txt', 'w')


@profile(stream=bubble_memory_usage)
def bubble_sort(data):
    swapped = True
    while swapped:
        swapped = False
        for _ in range(len(data) - 1):
            if data[_] > data[_ + 1]:
                data[_], data[_ + 1] = data[_ + 1], data[_]
                swapped = True
    return data


@profile(stream=insertion_memory_usage)
def insertion_sort(data):
    for i in range(1, len(data)):
        item_to_insert = data[i]
        j = i - 1
        while j >= 0 and data[j] > item_to_insert:
            data[j + 1] = data[j]
            j -= 1
        data[j + 1] = item_to_insert
    return data


@profile(stream=shell_memory_usage)
def shell_sort(data):
    last_index = len(data)
    step = len(data) // 2
    while step > 0:
        for i in range(step, last_index, 1):
            j = i
            delta = j - step
            while delta >= 0 and data[delta] > data[j]:
                data[delta], data[j] = data[j], data[delta]
                j = delta
                delta = j - step
        step //= 2
    return data


@profile(stream=quick_memory_usage)
def quicksort(array, low, high):
    if low < high:
        pi = partition(array, low, high)
        quicksort(array, low, pi - 1)
        quicksort(array, pi + 1, high)


def partition(array, low, high):
    pivot = array[high]
    i = low - 1
    for j in range(low, high):
        if array[j] <= pivot:
            i = i + 1
            (array[i], array[j]) = (array[j], array[i])
    (array[i + 1], array[high]) = (array[high], array[i + 1])
    return i + 1


data = [random.uniform(0.0, 100.0) * n for n in range(1000)]
data_bubble = data.copy()
data_insertion = data.copy()
data_shell = data.copy()
data_quick = data.copy()

print('---ПУЗЫРЬКОВАЯ СОРТИРОВКА---')
start_time = timeit.default_timer()
bubble_sort(data_bubble)
stop_time = timeit.default_timer()
print('Время работы', stop_time - start_time)

print('---СОРТИРОВКА ВСТАВКАМИ---')
start_time = timeit.default_timer()
insertion_sort(data_insertion)
stop_time = timeit.default_timer()
print('Время работы', stop_time - start_time)

print('---СОРТИРОВКА ШЕЛЛА---')
start_time = timeit.default_timer()
shell_sort(data_shell)
stop_time = timeit.default_timer()
print('Время работы', stop_time - start_time)

print('---БЫСТРАЯ СОРТИРОВКА---')
start_time = timeit.default_timer()
quicksort(data_quick, 0, len(data_quick)-1)
stop_time = timeit.default_timer()
print('Время работы', stop_time - start_time)

bubble_memory_usage.close()
insertion_memory_usage.close()
shell_memory_usage.close()
quick_memory_usage.close()
