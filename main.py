import random

data = [random.uniform(0.0, 100.0) * n for n in range(50)]


def bubble_sort(data):
    swapped = True
    while swapped:
        swapped = False
        for _ in range(len(data)-1):
            if data[_] > data[_ + 1]:
                data[_], data[_ + 1] = data[_ + 1], data[_]
                swapped = True
    return data


def insertion_sort(data):
    for i in range(1, len(data)):
        item_to_insert = data[i]
        j = i - 1
        while j >= 0 and data[j] > item_to_insert:
            data[j + 1] = data[j]
            j -= 1
        data[j + 1] = item_to_insert
    return data


def shell_sort(data):
    last_index = len(data)
    step = len(data) // 2
    while step > 0:
        for i in range(step, last_index, 1):
            j = i
            delta = j - step
            while delta >= 0 and data[delta] > data[j]:
                data[delta], data[j] = data[j], data[delta]
                j = delta
                delta = j - step
        step //= 2
    return data


def partition(array, low, high):
    pivot = array[high]
    i = low - 1
    for j in range(low, high):
        if array[j] <= pivot:
            i = i + 1
            (array[i], array[j]) = (array[j], array[i])
    (array[i + 1], array[high]) = (array[high], array[i + 1])
    return i + 1


def quicksort(array, low, high):
    if low < high:
        pi = partition(array, low, high)
        quicksort(array, low, pi - 1)
        quicksort(array, pi + 1, high)
